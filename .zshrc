#----------- Source Files --------{{{1
# ohmyzsh
source $ZDOTDIR/ohmyzsh.zsh
# zsh-autosuggestions
source $ZDOTDIR/zsh-autosuggestions/zsh-autosuggestions.zsh
# zsh-syntax-highlighting
source $ZDOTDIR/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh


#----------- Variables -----------{{{1
export MANPATH="/usr/local/man:$MANPATH"
# export LANG=en_US.UTF-8
# export LANGUAGE=jp_JP.UTF-8
# export LC_ALL=jp_JP.UTF-8
# Preferred editor for local and remote sessions
if [[ -n $SSH_CONNECTION ]]; then
  export EDITOR='vim'
else
  export EDITOR='mvim'
fi
# History config
HISTSIZE=10000
SAVEHIST=10000


#----------- Vim-like Setting -----------{{{1
# Use vim keys in tab complete menu:
bindkey -M menuselect 'h' vi-backward-char
bindkey -M menuselect 'k' vi-up-line-or-history
bindkey -M menuselect 'l' vi-forward-char
bindkey -M menuselect 'j' vi-down-line-or-history
bindkey -v '^?' backward-delete-char

export KEYTIMEOUT=20

# 'jk' to enter normal mode
bindkey -M viins 'jk' vi-cmd-mode 

# Change cursor shape for different vi modes.
function zle-keymap-select {
  if [[ ${KEYMAP} == vicmd ]] ||
     [[ $1 = 'block' ]]; then
    echo -ne '\e[1 q'

  elif [[ ${KEYMAP} == main ]] ||
       [[ ${KEYMAP} == viins ]] ||
       [[ ${KEYMAP} = '' ]] ||
       [[ $1 = 'beam' ]]; then
    echo -ne '\e[5 q'
  fi
}
zle -N zle-keymap-select

zle-line-init() {
    zle -K viins # initiate `vi insert` as keymap (can be removed if `bindkey -V` has been set elsewhere)
    echo -ne '\e[5 q'
}
zle -N zle-line-init


#----------- Path -----------{{{1
# Miniconda
if [ -d "$HOME/.local/share/miniconda3/bin" ]; then
    export PATH="$HOME/.local/share/miniconda3/bin:$PATH"
fi


#----------- File Locations -----------{{{1
# History File
HISTFILE=$ZDOTDIR/.zsh_history


#----------- Aliases -------------{{{1
alias la='ls -la'
# Dotfile Config
alias config='/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'
# Vim
alias v='vim'
# Vivado
if [ -d "/tools/Xilinx/Vivado/2020.1/bin" ]; then
    alias vivado="/tools/Xilinx/Vivado/2020.1/bin/vivado -nolog -nojournal"
fi


#----------- Others -----------{{{1
# Opam
test -r /home/tatsu/.opam/opam-init/init.zsh && . /home/tatsu/.opam/opam-init/init.zsh > /dev/null 2> /dev/null || true
